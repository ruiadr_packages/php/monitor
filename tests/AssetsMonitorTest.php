<?php

namespace Ruiadr\Monitor\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Monitor\AssetsMonitor;

final class AssetsMonitorTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    private function assertByType(array $assets, string $type): void
    {
        $this->assertIsArray($assets['types'][$type]);
        $this->assertIsNumeric($assets['types'][$type]['errors']);
        $this->assertIsArray($assets['types'][$type]['collection']);
        $this->assertTrue(count($assets['types'][$type]['collection']) > 0);

        foreach ($assets['types'][$type]['collection'] as $collection) {
            $this->assertIsString($collection['url']);
            $this->assertIsString($collection['type']);
            $this->assertIsNumeric($collection['code']);
        }
    }

    public function testUrl(): void
    {
        $assets = AssetsMonitor::buildFromUrlString(self::TEST_DOMAIN)->getCollection();

        $this->assertIsArray($assets);
        $this->assertIsNumeric($assets['errors']);
        $this->assertIsArray($assets['types']);

        $this->assertByType($assets, 'javascript');
        $this->assertByType($assets, 'stylesheets');
    }

    public function testType(): void
    {
        $assets = AssetsMonitor::buildFromUrlString(self::TEST_DOMAIN);

        $this->assertIsArray($assets->getTypes());
        $this->assertContains('javascript', $assets->getTypes());
        $this->assertContains('stylesheets', $assets->getTypes());
    }
}
