<?php

namespace Ruiadr\Monitor;

use Ruiadr\Base\Http\Interface\HttpInterface;
use Ruiadr\Base\Wrapper\Reflection;
use Ruiadr\Base\Wrapper\Url;
use Ruiadr\Fetcher\Base\FetcherBase;
use Ruiadr\Fetcher\HtmlFetcher;
use Ruiadr\Monitor\Interface\AssetsMonitorInterface;

class AssetsMonitor implements AssetsMonitorInterface
{
    private ?string $content = null;
    private ?array $collection = null;

    /**
     * @param Url $url URL de la page à analyser
     */
    public function __construct(private readonly Url $url)
    {
    }

    final public static function buildFromUrlString(string $urlString): AssetsMonitorInterface
    {
        return new static(new Url($urlString));
    }

    final public function getTypes(): array
    {
        return [
            // Doit correspondre à des parseurs existants, sinon ils seront
            // ignorés par la classe.
            'javascript',
            'stylesheets',
        ];
    }

    /**
     * Retourne le contenu HTML de l'URL qui a servi à construire
     * l'objet courant. Le résultat est mis en cache.
     *
     * @return string Contenu de la page HTML à parser
     */
    private function getContent(): string
    {
        return $this->content ??= (new HtmlFetcher($this->url))->getContent();
    }

    /**
     * Nettoyage de $urlString passée en paramètre en s'appuyant sur l'objet
     * Url qui a servi à la construction de l'objet courant.
     * L'idée est de reconstruire une URL complète contenant toutes
     * les parties requises: scheme, host, etc.
     *
     * @param string $urlString URL à nettoyer
     *
     * @return string URL nettoyée
     */
    private function cleanUrl(string $urlString): string
    {
        $url = new Url($urlString);

        if (null === $url->host) {
            $urlString = $this->url->host.$urlString;
        }

        if (null === $url->scheme) {
            if (str_starts_with($urlString, '//')) {
                $urlString = substr($urlString, 2);
            }
            $urlString = $this->url->scheme.'://'.$urlString;
        }

        return $urlString;
    }

    /**
     * Retourne true, si l'URL $urlString passée en paramètre possède le même host
     * que l'objet Url qui a servi à la construction de l'objet courant.
     *
     * @param string $urlString URL à valider
     *
     * @return bool true si $urlString est du même domaine que l'URL de l'objet
     */
    private function isCurrentDomain(string $urlString): bool
    {
        return (new Url($urlString))->domain === $this->url->domain;
    }

    /**
     * Retourne une liste d'assets de type $type passé en paramètre, depuis
     * l'URL de la page qui a servi à la construction de l'objet courant.
     *
     * @param string $type Type de ressource à parser
     *
     * @return array Éléments parsés
     */
    private function buildCollectionSource(string $type): array
    {
        $class = Reflection::buildSuffix($type, 'parser');

        return $class instanceof \ReflectionClass
            ? $class->newInstance($this->getContent())->getCollection()
            : [];
    }

    /**
     * Retourne des informations sur tous les assets de type $type
     * qui ont été trouvés sur la page de l'URL qui a servi à la construction
     * de l'objet courant.
     *
     * @param string $type Type de ressource à parser
     *
     * @return array Informations sur les ressources parsées
     */
    private function getCollectionInfos(string $type): array
    {
        $result = [
            'errors' => 0,
            'collection' => [],
        ];

        foreach ($this->buildCollectionSource($type) as $url) {
            $url = $this->cleanUrl($url);

            if (!$this->isCurrentDomain($url)) {
                continue;
            }

            $fetcher = FetcherBase::buildFetcher($type, new Url($url));

            $code = $fetcher->getResponseCode();
            if (is_int($code) && HttpInterface::STATUS_SUCCESS_OK !== $code) {
                ++$result['errors'];
            }

            $result['collection'][] = [
                'url' => $url,
                'type' => $fetcher->getContentType(),
                'code' => $code,
            ];
        }

        return $result;
    }

    final public function getCollection(): array
    {
        if (null === $this->collection) {
            $this->collection = [
                'errors' => 0,
                'types' => [],
            ];

            foreach ($this->getTypes() as $type) {
                $this->collection['types'][$type] = $this->getCollectionInfos($type);
                $this->collection['errors'] += $this->collection['types'][$type]['errors'];
            }
        }

        return $this->collection;
    }
}
