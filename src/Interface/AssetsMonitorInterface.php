<?php

namespace Ruiadr\Monitor\Interface;

interface AssetsMonitorInterface
{
    /**
     * Construction de l'objet à partir de la chaîne $urlString
     * passée en paramètre.
     *
     * @param string $urlString URL à parser
     *
     * @return AssetsMonitorInterface Instance de type AssetsMonitorInterface
     */
    public static function buildFromUrlString(string $urlString): AssetsMonitorInterface;

    /**
     * Éléments à parser dans la page. Le nom des éléments doit
     * correspondre à des parseurs existants.
     *
     * @return array Types qui seront parsés dans l'URL qui a servi à construire l'objet
     */
    public function getTypes(): array;

    /**
     * Retourne une liste contenant tous les assets trouvés sur la page
     * de l'URL qui a servi à la construction de l'objet courant.
     *
     * Le tableau est rangé par type d'asset, ex:
     * [
     *      'errors' => xxx,
     *      'types' => [
     *          'javascript' => [
     *              'errors' => xxx,
     *              'collection' => [
     *                  [ 'url' => 'xxx', 'type' => 'xxx', 'code' => 'xxx', ],
     *                  [ 'url' => 'xxx', 'type' => 'xxx', 'code' => 'xxx', ],
     *              ]
     *          ],
     *          'stylesheets' => [
     *              'errors' => xxx,
     *              'collection' => [
     *                  [ 'url' => 'xxx', 'type' => 'xxx', 'code' => 'xxx', ],
     *                  [ 'url' => 'xxx', 'type' => 'xxx', 'code' => 'xxx', ],
     *              ]
     *          ],
     *      ]
     * ].
     *
     * @return array Éléments récupérés de l'URL parsée
     */
    public function getCollection(): array;
}
